//
//  Tree.m
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import "Tree.h"

@interface Tree ()

+ (NSString *)getStringWithFixedLength:(NSString *)string andLength:(int)length;

@end

@implementation Tree

- (id)init
{
    self = [super init];
    if (self)
    {
        self.root = [[TreeNode alloc] initWithTitle:@"Root"];
    }
    return self;
}

#pragma mark - Public Methods

- (void)insertTreeNode:(TreeNode *)treeNode toParent:(TreeNode *)treeNodeParent
{
    if (!treeNode) return;
    if (!treeNodeParent) return;
    
    treeNode.parentTreeNode = treeNodeParent;
    
    if (![treeNodeParent.mutableArraySubTreeNodes containsObject:treeNode])
    {
        [treeNodeParent.mutableArraySubTreeNodes addObject:treeNode];
    }
}

- (void)insertTreeNode:(TreeNode *)treeNode toParent:(TreeNode *)treeNodeParent atPosition:(int)position
{
    if (!treeNode) return;
    if (!treeNodeParent) return;
    
    treeNode.parentTreeNode = treeNodeParent;
    
    if (![treeNodeParent.mutableArraySubTreeNodes containsObject:treeNode])
    {
        [treeNodeParent.mutableArraySubTreeNodes insertObject:treeNode atIndex:position];
    }
}

- (void)removeTreeNode:(TreeNode *)treeNode
{
    if (!treeNode) return;
    
    NSArray *tmpArraySubTreeNodes = [treeNode.mutableArraySubTreeNodes copy];
    
    for (TreeNode *tmpTreeNode in tmpArraySubTreeNodes)
    {
        [self removeTreeNode:tmpTreeNode];
    }
    
    [treeNode.mutableArraySubTreeNodes removeAllObjects];
    
    if (treeNode.parentTreeNode)
    {
        [treeNode.parentTreeNode.mutableArraySubTreeNodes removeObject:treeNode];
    }
    
    treeNode.parentTreeNode = nil;
}

- (TreeNode *)removeTreeNodeFromParentNode:(TreeNode *)treeNode
{
    if (!treeNode) return nil;
    
    TreeNode *tmpNodePrarent = treeNode.parentTreeNode;
    
    if (tmpNodePrarent)
    {
        if ([tmpNodePrarent.mutableArraySubTreeNodes containsObject:treeNode])
            [tmpNodePrarent.mutableArraySubTreeNodes removeObject:treeNode];
    }
    
    treeNode.parentTreeNode = nil;
    
    return treeNode;
}

- (void)PrintTreeBefore:(TreeNode *)treeNode
{
    if (!treeNode) return;
    
    int level = [treeNode getLevel];
    NSString *tmpStrSpaces = [Tree getStringWithFixedLength:@"-" andLength:level];
    NSLog(@"%@%d: %@", tmpStrSpaces, level, treeNode.mutableDictContent[@"title"]);
    
    for (TreeNode *tmpTreeNode in treeNode.mutableArraySubTreeNodes)
    {
        [self PrintTreeBefore:tmpTreeNode];
    }
}

- (void)PrintTreeAfter:(TreeNode *)treeNode
{
    if (!treeNode) return;
    
    for (TreeNode *tmpTreeNode in treeNode.mutableArraySubTreeNodes)
    {
        [self PrintTreeAfter:tmpTreeNode];
    }
    
    int level = [treeNode getLevel];
    NSString *tmpStrSpaces = [Tree getStringWithFixedLength:@"-" andLength:level];
    NSLog(@"%@%d: %@", tmpStrSpaces, level, treeNode.mutableDictContent[@"title"]);
}

- (NSArray *)getAllTreeNode:(TreeNode *)treeNode
{
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    if (!treeNode) return tmpMutableArray;
    
    [tmpMutableArray addObject:treeNode];
    
    for (TreeNode *tmpTreeNode in treeNode.mutableArraySubTreeNodes)
    {
        NSArray *tmpArray = [self getAllTreeNode:tmpTreeNode];
        
        [tmpMutableArray addObjectsFromArray:tmpArray];
    }
    
    return tmpMutableArray;
}

- (BOOL)nodeInTheTree:(TreeNode *)treeNode andRootOfTheTree:(TreeNode *)root
{
    TreeNode *tmpNode = treeNode;
    
    while (tmpNode != root)
    {
        tmpNode = tmpNode.parentTreeNode;
        
        if (!tmpNode) return NO;
    }
    
    return YES;
}

- (NSArray *)getTreeNodesByTitle:(NSString *)title andParentTitle:(NSString *)parentTitle
{
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    NSArray *tmpArray = [self getAllTreeNode:self.root];
    
    for (TreeNode *tmpNode in tmpArray)
    {
        NSString *tmpStrTitle = [tmpNode getTitle];
        NSString *tmpStrParentTitle = [tmpNode.parentTreeNode getTitle];
        if ([tmpStrTitle isEqualToString:title])
        {
            if (parentTitle)
            {
                if ([tmpStrParentTitle isEqualToString:parentTitle]) [tmpMutableArray addObject:tmpNode];
            }else
                [tmpMutableArray addObject:tmpNode];
        }
    }
    
    return tmpMutableArray;
}

- (NSArray *)getAllUnfoldedTreeNodeUnderTreeNode:(TreeNode *)treeNode andItself:(BOOL)bInclude
{
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    if (!treeNode) return tmpMutableArray;
    
    if (bInclude) [tmpMutableArray addObject:treeNode];
    
    if (treeNode.bFolded) return tmpMutableArray;
    
    for (TreeNode *tmpTreeNode in treeNode.mutableArraySubTreeNodes)
    {
        [tmpMutableArray addObject:tmpTreeNode];
        
        if (!tmpTreeNode.bFolded)
        {
            NSArray *tmpArray = [self getAllUnfoldedTreeNodeUnderTreeNode:tmpTreeNode andItself:NO];
            [tmpMutableArray addObjectsFromArray:tmpArray];
        }
    }
    
    return tmpMutableArray;
}

- (TreeNode *)getTreeNodeByIdentifier:(NSString *)identifier
{
    NSArray *tmpArray = [self getAllTreeNode:self.root];
    
    for (TreeNode *tmpNode in tmpArray)
    {
        NSString *tmpStrIdentifier = tmpNode.identifier;
        if ([tmpStrIdentifier isEqualToString:identifier])
            return tmpNode;
    }
    
    return nil;
}

#pragma mark - Public Static Methods

+ (NSArray *)arrayWithAlphabeticalOrder:(NSArray *)arrayTreeNodes
{
    if (!arrayTreeNodes) return nil;
    
    NSArray *tmpArray = [arrayTreeNodes sortedArrayUsingComparator:^(TreeNode *treeNode1, TreeNode *treeNode2){
        
        NSString *tmpStrTitle1 = [treeNode1 getTitle];
        NSString *tmpStrTitle2 = [treeNode2 getTitle];
        
        return [tmpStrTitle1 compare:tmpStrTitle2];
    }];
    
    return tmpArray;
}

+ (NSString *)getStringWithFixedLength:(NSString *)string andLength:(int)length
{
    NSMutableString *tmpMutableString = [[NSMutableString alloc] initWithString:@""];
    
    if (tmpMutableString.length <= length)
    {
        int numberOfSpaces = length - tmpMutableString.length;
        for (int i = 1; i <= numberOfSpaces; i++)
        {
            [tmpMutableString appendString:string];
        }
    }
    
    return tmpMutableString;
}

@end
