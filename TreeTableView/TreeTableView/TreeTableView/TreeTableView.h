//
//  TreeTableView.h
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tree.h"
#import "TreeNode.h"
#import "TreeTableViewDelegate.h"

@interface TreeTableView : UITableView

@property (weak, nonatomic) id <TreeTableViewDelegate> delegateForTheTreeTableView;

- (BOOL)RemoveNode:(TreeNode *)node;
- (BOOL)AddNode:(TreeNode *)node toParentNode:(TreeNode *)parentNode andIndexPaths:(NSMutableArray *)indexPaths;
- (BOOL)AddNode:(TreeNode *)node toParentNode:(TreeNode *)parentNode andIndexPaths:(NSMutableArray *)indexPaths atIndex:(int)position;
- (BOOL)FoldNode:(TreeNode *)node;
- (BOOL)UnfoldNode:(TreeNode *)node;
- (void)beginOperation;
- (void)endOperation;
- (void)RefreshModel;

- (TreeTableViewCell *)getVisibleCellByNode:(TreeNode *)theTreeNode;
- (NSArray *)getAllVisibleCells;
- (int)getRowPositionForNode:(TreeNode *)node;

- (Tree *)getTree;
- (void)setNewTree:(Tree *)tree;

@end
