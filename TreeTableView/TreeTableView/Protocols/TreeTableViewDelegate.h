//
//  TreeTableViewDelegate.h
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TreeTableView, Tree, TreeTableViewCell;

@protocol TreeTableViewDelegate <NSObject>

- (Tree *)TreeForTheTreeTableView:(TreeTableView *)theTreeTableView;
- (TreeTableViewCell *)cellForTreeTableView:(TreeTableView *)theTreeTableView andTreeNode:(TreeNode *)theTreeNode;

@optional

- (void)TreeTableView:(TreeTableView *)theTreeTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)TreeTableView:(TreeTableView *)theTreeTableView didTapIconWithNode:(TreeNode *)theTreeNode;
- (void)TreeTableView:(TreeTableView *)theTreeTableView didSwipeToRightWithNode:(TreeNode *)theTreeNode;
- (void)TreeTableView:(TreeTableView *)theTreeTableView didSwipeToLeftWithNode:(TreeNode *)theTreeNode;
- (void)TreeTableView:(TreeTableView *)theTreeTableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath;
- (BOOL)TreeTableView:(TreeTableView *)theTreeTableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)TreeTableView:(TreeTableView *)theTreeTableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)TreeTableView:(TreeTableView *)theTreeTableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath;
- (UITableViewCellEditingStyle)TreeTableView:(TreeTableView *)theTreeTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSIndexPath *)TreeTableView:(TreeTableView *)theTreeTableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath;
- (void)TreeTableView:(TreeTableView *)theTreeTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;

@end
