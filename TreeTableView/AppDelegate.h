//
//  AppDelegate.h
//  TreeTableView
//
//  Created by Ricol Wang on 15/06/2016.
//  Copyright © 2016 Ricol Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

