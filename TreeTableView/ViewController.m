//
//  ViewController.m
//  TreeTableView
//
//  Created by Ricol Wang on 15/06/2016.
//  Copyright © 2016 Ricol Wang. All rights reserved.
//

#import "ViewController.h"
#import "TreeTableView.h"
#import "TreeTableViewDelegate.h"

#define MAX_LEVEL 10
#define MAX_MENUS 10

@interface ViewController () <TreeTableViewDelegate>

@property (weak, nonatomic) IBOutlet TreeTableView *theTableView;
@property (weak, nonatomic) IBOutlet UISwitch *theSwitch;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"TreeTableView Demo";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.theTableView.delegateForTheTreeTableView = self;
    [self.theTableView reloadData];
}

#pragma mark - TreeTableViewDelegate

- (TreeTableViewCell *)cellForTreeTableView:(TreeTableView *)theTreeTableView andTreeNode:(TreeNode *)theTreeNode
{
    TreeTableViewCell *cell = theTreeNode.cell;
    if (!cell)
    {
        cell = [[TreeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    int level = [theTreeNode getLevel];
    NSMutableString *text = [NSMutableString new];
    for (int i = 0; i < level; i++)
    {
        [text appendString:@"       "];
    }
    [text appendString:[theTreeNode getTitle]];
    if (theTreeNode.mutableArraySubTreeNodes.count > 0)
        [text appendFormat:@" [%d]", theTreeNode.mutableArraySubTreeNodes.count];
    cell.textLabel.text = text;
    
    return cell;
}

- (Tree *)TreeForTheTreeTableView:(TreeTableView *)theTreeTableView
{
    return [self rebuildTree];
}

- (void)TreeTableView:(TreeTableView *)theTreeTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeTableViewCell *cell = [theTreeTableView cellForRowAtIndexPath:indexPath];
    TreeNode *node = cell.node;
    if (node.bFolded)
        [theTreeTableView UnfoldNode:node];
    else
        [theTreeTableView FoldNode:node];
}

#pragma mark - Private Methods

- (Tree *)rebuildTree
{
    Tree *theTree = [[Tree alloc] init];
    
    int maxI = arc4random() % 10 + 5;
    for (int i = 0; i < maxI; i++)
    {
        TreeNode *aNode = [[TreeNode alloc] initWithTitle:[NSString stringWithFormat:@"%d", i]];
        [theTree insertTreeNode:aNode toParent:theTree.root];
        
        [self insertNewMenu:aNode andTree:theTree];
    }
    
    return theTree;
}

- (void)insertNewMenu:(TreeNode *)parent andTree:(Tree *)theTree
{
    if ([parent getLevel] >= MAX_LEVEL) return;
    if (arc4random() % 100 > 40)
    {
        int max = arc4random() % MAX_MENUS;
        for (int i = 0; i < max; i++)
        {
            NSMutableString *text = [NSMutableString new];
            [text appendString:[parent getTitle]];
            [text appendFormat:@"-%d", i];
            TreeNode *aNewNode = [[TreeNode alloc] initWithTitle:text];
            [theTree insertTreeNode:aNewNode toParent:parent];
            
            if (arc4random() % 10 > 5)
                [self insertNewMenu:aNewNode andTree:theTree];
        }
    }
    
    parent.bFolded = !self.theSwitch.isOn;
}

#pragma mark - IBAction Methods

- (IBAction)btnRefreshOnTapped:(id)sender
{
    [self.theTableView setNewTree:[self rebuildTree]];
}

@end
